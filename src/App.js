import React, {Fragment, useState, useEffect} from 'react';
import Header from './components/Header';
import Formulario from './components/Formulario';
import Listado from './components/Listado';


function App() {

  const [monedas, consultarMonedas] = useState('valoresprincipales');
  const [tasas, guardarTasas]=useState([]);

  //UseEffect para consultar la api

  useEffect(() => {
    const consultarAPI= async() =>{
      const url = `https://www.dolarsi.com/api/api.php?type=${monedas}`;
      const respuesta = await fetch(url);
      const tasas = await respuesta.json();
      guardarTasas(tasas);
    } 
    consultarAPI();
  }, [monedas]);
  
  return (
    <Fragment>
      <Header/>
      <div className="contenido">
        <div className="formulario">
          <Formulario
            consultarMonedas={consultarMonedas}
          />
        </div>
      </div>
      <div>
        <div className="listado">
            <Listado
              tasas={tasas}
            />
        </div>
      </div>
    </Fragment>
  );
}

export default App;
