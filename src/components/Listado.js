import React,{Fragment} from 'react';
import Tasa from './Tasa';


const Listado = ({tasas}) => {

    return ( 
        <Fragment>
        <div className="contenido-tasas">
            {tasas.map(tasa=>(
                <Tasa
                    tasa={tasa}
                    key={tasa.casa.nombre}
                />
            ))}
        </div>
        </Fragment>

     );
}
 
export default Listado;