import React,{useState} from 'react';

const Formulario = ({consultarMonedas}) => {
    //State que selecciona la moneda
    const [moneda, actualizarMoneda]=useState('');

    const seleccionarModena=e=>{
        e.preventDefault();

        //comunicamos con el componente principal
        consultarMonedas(moneda);
    }

    
    const monedas=[
        {tipo:'valoresprincipales', nombre:'Dolar'},
        {tipo:'euro', nombre:'Euro'},
        {tipo:'real', nombre:'Real'}
    ];

    return ( 
        <form
            onSubmit={seleccionarModena}
        >
            <h3>Consultar:</h3>
            <select
                className="select"
                onChange={e=>actualizarMoneda(e.target.value)}
            >
                
                {monedas.map(moneda=>(
                    <option 
                        key={moneda.tipo}
                        value={moneda.tipo}
                    >{moneda.nombre}</option>
                ))}
            </select>
            <button 
                className="boton"
                type="submit"
            >CONSULTAR</button>
        </form>
     );
}
 
export default Formulario;