import React,{Fragment} from 'react';

const Tasa = ({tasa}) => {

    const {compra, venta, nombre, variacion}= tasa.casa;

    const varianza = (!variacion || variacion==='0') ? null : <div className="variacion"><p>{variacion}%</p></div>;

    const dolar = (nombre==='Dolar') ? 'Dolar Promedio' : nombre;

    const riesgoPais = (nombre==='Argentina') 
        ? <div className="tasa-cuerpo"><p className="riesgo">Riesgo Pais</p><p className="riesgo-cantidad">{compra}</p></div>
        : <div className="tasa-cuerpo">
                <div className="venta-compra">
                    <p className="texto">Compra</p>
                    <p className="cantidad">${compra}</p>
                </div>
                <div className="venta-compra">
                    <p className="texto">Venta</p>
                    <p className="cantidad">${venta}</p>
                </div>
           </div>

    const mitasa = (compra==='No Cotiza') 
    
    ? null 
    
    :   <div className="tasa">
            <div className="tasa-header">
                <p className="tasa-nombre">{dolar}</p>
            </div>
            {riesgoPais}
            {varianza}
        </div>; 
    
    return ( 
        <Fragment>
            {mitasa}
        </Fragment>
     );
}
 
export default Tasa;